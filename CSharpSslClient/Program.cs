﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSslClient
{
    class Program
    {
        public Program()
        {
            X509Certificate2 certificates = new X509Certificate2();

            certificates.Import(File.ReadAllBytes(@"c:\program files\javelin\certs\core.p12"), "javelin!Q2w3e4r", X509KeyStorageFlags.DefaultKeySet);
            //p12 generate from device.key & device.crt

            var client = new RestClient("https://WIN-G2D5IVQ7SU3/dmm");
            var request = new RestRequest("Register/Hello", Method.GET);

            client.ClientCertificates = new X509CertificateCollection() { certificates };

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            Console.WriteLine(content);
        }

        static void Main(string[] args)
        {
            new Program();
        }
    }
}
